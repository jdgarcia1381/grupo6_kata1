# -*- coding: utf-8 -*-
from unittest import TestCase

from src.kata1 import kata1


class TestKata1(TestCase):
    def test_calcular_num(self):
        self.assertEquals(kata1().calcular_num(""), 0, "Número de elementos incorrecto")
        self.assertEquals(kata1().calcular_num("1"), 1, "Número de elementos incorrecto")
        self.assertEquals(kata1().calcular_num("1,2"), 2, "Número de elementos incorrecto")
        self.assertEquals(kata1().calcular_num("1,2,3"), 3, "Número de elementos incorrecto")

    def test_calcular_min(self):
        self.assertEquals(kata1().calcular_min(""), 0, "Número mínimo incorrecto")
        self.assertEquals(kata1().calcular_min("1"), 1, "Número mínimo incorrecto")
        self.assertEquals(kata1().calcular_min("1,2"), 1, "Número mínimo incorrecto")
        self.assertEquals(kata1().calcular_min("7,2,3"), 2, "Número mínimo incorrecto")

    def test_calcular_max(self):
        self.assertEquals(kata1().calcular_max(""), 0, "Número máximo incorrecto")
        self.assertEquals(kata1().calcular_max("1"), 1, "Número máximo incorrecto")
        self.assertEquals(kata1().calcular_max("1,2"), 2, "Número máximo incorrecto")
        self.assertEquals(kata1().calcular_max("4,32,87,5"), 87, "Número máximo incorrecto")

    def test_calcular_prom(self):
        self.assertEquals(kata1().calcular_prom(""), 0, "Promedio incorrecto")
        self.assertEquals(kata1().calcular_prom("1"), 1, "Promedio incorrecto")
        self.assertEquals(kata1().calcular_prom("1,2"), 1.5, "Promedio incorrecto")
        self.assertEquals(kata1().calcular_prom("10,5,15"), 10, "Promedio incorrecto")

    def test_calcular_estadistica(self):
        self.assertEquals(kata1().calcular_estadistica(""), [0, 0, 0, 0], "Respuesta incorrecta")
        self.assertEquals(kata1().calcular_estadistica("1"), [1, 1, 1, 1], "Respuesta incorrecta")
        self.assertEquals(kata1().calcular_estadistica("1,2"), [2, 1, 2, 1.5], "Respuesta incorrecta")
        self.assertEquals(kata1().calcular_estadistica("1,2,3"), [3, 1, 3, 2], "Respuesta incorrecta")
