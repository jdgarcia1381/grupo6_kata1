# -*- coding: utf-8 -*-
class kata1:
    def calcular_num(self, cadena):
        cadena = str(cadena)
        numero_elementos = 0
        if len(cadena) > 0:
            if len(cadena) == 1:
                numero_elementos = 1
            else:
                cadena_split = cadena.split(",")
                numero_elementos = len(cadena_split)
        return numero_elementos

    def calcular_min(self, cadena):
        cadena = str(cadena)
        minimo = 0
        if len(cadena) > 0:
            if len(cadena) == 1:
                minimo = int(cadena)
            else:
                cadena_split = cadena.split(",")
                minimo = int(cadena_split[0])
                for numero in cadena_split:
                    numero = int(numero)
                    if (numero < minimo):
                        minimo = numero
        return minimo

    def calcular_max(self, cadena):
        cadena = str(cadena)
        maximo = 0
        if len(cadena) > 0:
            if len(cadena) == 1:
                maximo = int(cadena)
            else:
                cadena_split = cadena.split(",")
                for numero in cadena_split:
                    numero = int(numero)
                    if (numero > maximo):
                        maximo = numero
        return maximo

    def calcular_prom(self, cadena):
        cadena = str(cadena)
        promedio = 0
        if len(cadena) > 0:
            if len(cadena) == 1:
                promedio = int(cadena)
            else:
                suma = 0
                cadena_split = cadena.split(",")
                for numero in cadena_split:
                    numero = int(numero)
                    suma += numero
                promedio = (float(suma) / float(len(cadena_split)))
        return promedio

    def calcular_estadistica(self, cadena):
        estadisticas = []
        estadisticas.append(self.calcular_num(cadena))
        estadisticas.append(self.calcular_min(cadena))
        estadisticas.append(self.calcular_max(cadena))
        estadisticas.append(self.calcular_prom(cadena))
        return estadisticas